<div class="widget-bg">
    <div class="widget-heading bg-score">
            <span class="widget-title my-0 color-white fs-15 fw-600">
                Carteras
            </span>
        <div class="pull-right">
            <a class="btn btn-success btn-sm" onclick="responseModal('div.dialogScoreExtraLarge','formCarteras')" data-toggle="modal" data-target="#modalScore" data-backdrop="static">
                <i class="list-icon material-icons text-inverse">add</i>
            </a>
        </div>
    </div>
    <div class="widget-body border_fix">
        <table id="listCarteras" class="table dt-responsive nowrap">
            <thead class="bg-primary">
            <tr>
                <th>ID</th>
                <th>Cartera</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadCarteraDatatable()
    })

    function loadCarteraDatatable(){
        dataTables('listCarteras', '/listCarteras', { idProveedor: '{{ $idProovedor }}' }, false, false)
    }
</script>