<div class="widget-holder widget-sm widget-border-radius col-md-5">
    @include('elements.negocio.cards.proveedores')
</div>

<div id="loadingCartera" class="widget-holder widget-sm widget-border-radius col-md-5 disabled">
    @include('elements.negocio.cards_blank.carteras_blank')
</div>
<div id="requestCartera" class="widget-holder widget-sm widget-border-radius col-md-5 d-none"></div>

<div id="requestGrupoResultado" class="widget-holder widget-sm widget-border-radius col-md-5 disabled">
    @include('elements.negocio.cards_blank.grupo_resultado_blank')
</div>
<div id="requestResultado" class="widget-holder widget-sm widget-border-radius col-md-5 disabled">
    @include('elements.negocio.cards_blank.resultado_blank')
</div>