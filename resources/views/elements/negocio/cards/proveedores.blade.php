<div class="widget-bg">
    <div class="widget-heading bg-score">
            <span class="widget-title my-0 color-white fs-15 fw-600">
                Proveedores
            </span>
        <div class="pull-right">
            <a class="btn btn-success btn-sm" onclick="responseModal('div.dialogScoreLarge','formProveedores')" data-toggle="modal" data-target="#modalScore">
                <i class="list-icon material-icons text-inverse">add</i>
            </a>
        </div>
    </div>
    <div class="widget-body border_fix">
        <table id="listProveedores" class="table dt-responsive nowrap">
            <thead class="bg-primary">
            <tr>
                <th>ID</th>
                <th>Avatar</th>
                <th>Proveedor</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadProveedorDatatable()
    })

    function loadProveedorDatatable(){
        dataTables('listProveedores', '/listProveedores', {}, false, false)
    }
</script>