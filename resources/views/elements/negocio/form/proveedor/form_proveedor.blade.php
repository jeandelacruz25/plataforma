<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Proveedor</h5>
    </div>
    <div class="modal-body">
        <form id="formProveedor" enctype="multipart/form-data" class="mr-b-30">
            <div class="row">
                <div class="col-md-6 text-center">
                    <figure class="imgOverlay thumb-lg">
                        <img class="img-thumbnail" id="imageProveedor" src="{{ asset($dataProveedor ? $dataProveedor[0]['avatar'].'?version='.date('YmdHis') : 'img/proveedores/img-default.svg?version='.date('YmdHis')) }}" />
                        <div class="middle">
                            <div class="fileUpload btn btn-primary">
                                <span><i class="list-icon fa fa-upload"></i></span>
                                <input type="file" class="upload" onchange="showImagePreview(this, '#imageProveedor', 'avatarOriginalProveedor')" name="avatarProveedor" value="{{ $dataProveedor ? $dataProveedor[0]['avatar'] : '' }}"/>
                            </div>
                        </div>
                    </figure>
                    <p class="text-danger font-weight-bold">(128 x 128)</p>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Razón Social</label>
                        <input type="text" name="nombreProveedor" class="form-control" placeholder="Ingresa la razón social" value="{{ $dataProveedor ? $dataProveedor[0]['razon_social'] : '' }}">
                    </div>
                    <div class="form-group">
                        <label>Número de RUC</label>
                        <input type="text" name="rucProveedor" class="form-control" placeholder="Ingresa el número de RUC" value="{{ $dataProveedor ? $dataProveedor[0]['ruc'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" name="telefonoProveedor" class="form-control" placeholder="Ingrese el número de telefono" value="{{ $dataProveedor ? $dataProveedor[0]['telephone'] : '' }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Usuario de Contacto [Correo]</label>
                        <input type="text" name="emailProveedor" class="form-control" placeholder="Ingrese el correo del usuario de contacto" value="{{ $dataProveedor ? $dataProveedor[0]['contacto'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Dirección</label>
                        <input type="text" name="direccionProveedor" class="form-control" placeholder="Ingrese la dirección" value="{{ $dataProveedor ? $dataProveedor[0]['address_business'] : '' }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <input type="hidden" name="proveedorID" value="{{ $dataProveedor ? $dataProveedor[0]['id'] : '' }}">
                    <input type="hidden" name="userCreate" value="{{ $dataProveedor ? $dataProveedor[0]['user_cre'] : '' }}">
                    <input type="hidden" name="userUpdate" value="{{ $dataProveedor ? $dataProveedor[0]['user_upd'] : '' }}">
                    <input type="hidden" name="dateCreate" value="{{ $dataProveedor ? $dataProveedor[0]['created_at'] : '' }}">
                    <input type="hidden" name="dateUpdate" value="{{ $dataProveedor ? $dataProveedor[0]['updated_at'] : '' }}">
                    <input type="hidden" name="statusID" value="{{ $dataProveedor ? $dataProveedor[0]['id_status'] : '' }}">
                    <input type="hidden" name="avatarOriginalProveedor" value="{{ $dataProveedor ? $dataProveedor[0]['avatar'] : '' }}">
                    <div class="mr-b-30">
                        <button type="submit" class="btn btn-primary btnForm ripple">{!! $updateForm === true ? "<i class='list-icon feather feather-edit-2' aria-hidden='true'></i> Editar" : "<i class='list-icon feather feather-plus' aria-hidden='true'></i> Agregar" !!}</button>
                        <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                        <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formProveedor.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScoreLarge')
</script>