<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-facebook">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScore')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Cambiar Estado [{{ $dataProveedor[0]['razon_social'] }}]</h5>
    </div>
    <div class="modal-body">
        <form id="formProveedorStatus">
            <div class="form-group">
                <label>Recuerda que cambiaras el estado <span class="badge px-3 heading-font-family {{ $dataProveedor[0]['status']['color'] }}">{{ $dataProveedor[0]['status']['nombre'] }}</span> a :</label>
                <select name="statusProveedor" class="form-control selectpicker show-tick">
                    <option value="">Seleccione un estado</option>
                    @foreach($options as $key => $value)
                        <option data-content="<span class='badge px-3 heading-font-family {{ $value['color'] }}'>{{ mb_strtoupper($value['nombre']) }}</span>" value="{{ $value['id'] }}">{{ $value['nombre'] }}</option>
                    @endforeach
                </select>
            </div>
            <input type="hidden" name="proveedorID" value="{{ $dataProveedor[0]['id'] }}">
            <div class="text-center mr-b-30">
                <button type="submit" class="btn btn-primary btnForm ripple"><i class='list-icon feather feather-refresh-cw' aria-hidden='true'></i> Actualizar</button>
                <button type="button" class="btn btn-info btnLoad ripple d-none"><i class="list-icon fa fa-spin fa-spinner" aria-hidden="true"></i> Cargando</button>
                <button type="button" class="btn btn-default ripple" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal"><i class="list-icon feather feather-x" aria-hidden="true"></i> Cerrar</button>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-icon alert-danger border-danger formError d-none"></div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/form/formProveedor.js?version='.date('YmdHis')) !!}"></script>
<script>
    hideErrorForm('.formError')
    clearModalClose('modalScore', 'div.dialogScore')
    initSelectPicker('.selectpicker')
</script>