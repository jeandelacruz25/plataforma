<!-- Modal content-->
<div class="modal-content animated bounceInDown">
    <div class="modal-header text-inverse bg-primary">
        <button type="button" class="close" onclick="clearModalClose('modalScore', 'div.dialogScoreExtraLarge')" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">{{ $updateForm === true ? "Editar" : "Agregar" }} Cartera</h5>
    </div>
    <div class="modal-body">
        <form id="formProveedor">
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link tabGeneral" href="#General" data-toggle="tab" aria-expanded="true">
                            <span class="thumb-xxs">
                                <img class="list-icon" src="{{ asset('img/modules/general.svg?version='.date('YmdHis')) }}"> General
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tabVentana" href="#Ventana" data-toggle="tab" aria-expanded="true">
                            <span class="thumb-xxs">
                                <img class="list-icon" src="{{ asset('img/modules/ventana.svg?version='.date('YmdHis')) }}"> Ventana de Neg. y Filtros
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="General">
                        @include('elements.negocio.form.cartera.utils.general')
                    </div>
                    <div class="tab-pane" id="Ventana">
                        @include('elements.negocio.form.cartera.utils.ventana')
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.tabGeneral').click()
    })

    hideErrorForm('.formError')
    initSelectPicker('.selectpicker')
    clearModalClose('modalScore', 'div.dialogScoreExtraLarge')
</script>