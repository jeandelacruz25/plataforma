<div class="row">
    <div class="col-md-12">
        <div class="widget-holder widget-sm widget-border-radius">
            <div class="widget-bg">
                <div class="widget-body border_fix">
                    <div class="row">
                        <select name="supervisores[]" id="supervisoresSelect" multiple="multiple">
                            @foreach($getOptions['supervisores'] as $key => $value)
                                <option data-ms-image="{{ asset($value['user']['avatar']).'?version='.date('YmdHis') }}" value="{{ $value['user']['id'] }}">{{ $value['user']['primer_nombre'].' '.$value['user']['segundo_nombre'].' '.$value['user']['primer_apellido'].' '.$value['user']['segundo_apellido'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget-holder widget-sm widget-border-radius">
            <div class="widget-bg">
                <div class="widget-body border_fix">
                    <div class="row">
                        <select id="agentesSelect" multiple="multiple">
                            @foreach($getOptions['agentes'] as $key => $value)
                                <option data-ms-image="{{ asset($value['user']['avatar']).'?version='.date('YmdHis') }}" value="{{ $value['user']['id'] }}">{{ $value['user']['primer_nombre'].' '.$value['user']['segundo_nombre'].' '.$value['user']['primer_apellido'].' '.$value['user']['segundo_apellido'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($) {
        $('#supervisoresSelect').multiSelect({
            keepOrder: true,
            sortable: true,
            selectableHeader: '<div class="widget-heading bg-primary"><div class="col-md-7"><span class="widget-title my-0 color-white fs-15 fw-600">Supervisores</span></div><div class="col-md-5"><input type="text" id="supervisores" class="search-input form-control form-control-sm" autocomplete="off" placeholder="Buscar.."></div></div>',
            selectionHeader: '<div class="widget-heading bg-primary"><div class="col-md-7"><span class="widget-title my-0 color-white fs-15 fw-600">Superv. Seleccionados</span></div><div class="col-md-5"><input type="text" id="supervisoresSeleccionados" class="search-input form-control form-control-sm" autocomplete="off" placeholder="Buscar.."></div></div>',
            afterInit: function(ms){
                let that = this
                let selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)'
                let selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected'

                that.qs1 = $('#supervisores').quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    })

                that.qs2 = $('#supervisoresSeleccionados').quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    })
            },
            afterSelect: function(){
                this.qs1.cache()
                this.qs2.cache()
            },
            afterDeselect: function(){
                this.qs1.cache()
                this.qs2.cache()
            }
        })

        $('#agentesSelect').multiSelect({
            keepOrder: true,
            sortable: true,
            selectableHeader: '<div class="widget-heading bg-facebook"><div class="col-md-7"><span class="widget-title my-0 color-white fs-15 fw-600">Agentes</span></div><div class="col-md-5"><input type="text" id="agentes" class="search-input form-control form-control-sm" autocomplete="off" placeholder="Buscar.."></div></div>',
            selectionHeader: '<div class="widget-heading bg-facebook"><div class="col-md-7"><span class="widget-title my-0 color-white fs-15 fw-600">Agentes Seleccionados</span></div><div class="col-md-5"><input type="text" id="agentesSeleccionados" class="search-input form-control form-control-sm" autocomplete="off" placeholder="Buscar.."></div></div>',
            afterInit: function(ms){
                let that = this
                let selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)'
                let selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected'

                that.qs1 = $('#agentes').quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    })

                that.qs2 = $('#agentesSeleccionados').quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    })
            },
            afterSelect: function(){
                this.qs1.cache()
                this.qs2.cache()
            },
            afterDeselect: function(){
                this.qs1.cache()
                this.qs2.cache()
            }
        })
    })
</script>