<div id="tiempoEstadosVue" class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Limites para tiempos de estados de los agentes</label>
            <table class="table table-responsive table-bordered">
                <thead class="bg-primary">
                <tr>
                    <th>Estados</th>
                    <th>Tiempos (Min.)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($getOptions['tipoEstados'] as $key => $value)
                    <tr>
                        <td>{{ $value['estado'] }}</td>
                        <td><input name="{{ $value['id'] }}" class="form-control form-control-sm" value=""></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>&nbsp;</label>
            <div class="widget-holder widget-sm widget-border-radius">
                <div class="widget-bg">
                    <div class="widget-heading bg-primary">
                        <span class="widget-title my-0 color-white fs-12 fw-600">Notificar a Usuarios</span>
                        <div class="widget-heading-icon">
                            <div class="checkbox checkbox-default">
                                <label class="checkbox-checked">
                                    <input type="checkbox" name="notificarTiempoEstado" v-model="form.notificarTiempoEstado"> <span class="label-text checkbox-in-widget"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="widget-body border_fix">
                        <div v-bind:class="[form.notificarTiempoEstado ? 'form-group' : 'form-group disabled']">
                            <label>Usuarios a Notificar</label>
                            <select name="usuariosNotificar[]" class="form-control form-control-sm selectpicker show-tick">
                                <option value="">Seleccione a los usuarios</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/form/negocio/tiempoEstadosVue.js?version='.date('YmdHis'))!!}"></script>