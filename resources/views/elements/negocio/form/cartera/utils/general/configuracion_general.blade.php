<div id="configuracionGeneralVue" class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Tipo de Negocio</label>
            <select name="typeIdentity" class="form-control form-control-sm selectpicker show-tick">
                <option value="">Seleccione un tipo de negocio</option>
                @foreach($getOptions['tipoCartera'] as $key => $value)
                    <option value="{{ $value['id'] }}">{{ ucwords(mb_strtolower($value['tipo_cartera'])) }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Nombre de la Cartera</label>
            <input type="text" name="nombreCartera" class="form-control" placeholder="Ingrese el nombre de la cartera" value="">
        </div>
        <div class="form-group">
            <label>Observaciones</label>
            <input type="text" name="observacionCartera" class="form-control" placeholder="Ingrese una observacion de esta cartera" value="">
        </div>
        <div class="form-group">
            <label>Canales de Gestión</label>
            <select name="canalGestion" class="form-control form-control-sm selectpicker show-tick" multiple data-live-search="true" data-selected-text-format="count > 3" data-actions-box="true">
                @foreach($getOptions['tipoCanales'] as $key => $value)
                    <option value="{{ $value['id'] }}">{{ ucwords(mb_strtolower($value['tipo_canal'])) }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Tipo de Marcación</label>
            <select name="tipoMarcacion" class="form-control form-control-sm selectpicker show-tick" multiple data-live-search="true" data-selected-text-format="count > 3" data-actions-box="true">
                @foreach($getOptions['tipoMarcaciones'] as $key => $value)
                    <option value="{{ $value['id'] }}">{{ ucwords(mb_strtolower($value['tipo_marcacion'])) }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Proveedor VoIP</label>
            <select name="proveedorVoip" class="form-control form-control-sm selectpicker show-tick" data-live-search="true">
                <option value="">Seleccione su proveedor VoIP</option>
                @foreach($getOptions['proveedoresVoip'] as $key => $value)
                    <option value="{{ $value['id'] }}">{{ ucwords(mb_strtolower($value['proveedor_voip'])) }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mr-t-5">
            <label>Tiempo para ACW</label>
            <input type="text" name="tiempoACW" class="form-control" placeholder="Ingrese el tiempo para ACW" value="">
        </div>
        <div class="form-group">
            <label>Meta / Cantidad de ventas mensual</label>
            <input type="text" name="cantidadVentas" class="form-control" placeholder="Ingrese la cantidad de ventas mensual" value="">
        </div>
        <div class="form-group">
            <label>Cantidad de Canales para llamada</label>
            <input type="text" name="cantidadCanales" class="form-control" placeholder="Ingrese la cantidad para las llamadas" value="">
            <div class="checkbox checkbox-primary">
                <label class="checkbox-checked">
                    <input type="checkbox" name="notificarDesborde" v-model="form.notificarDesborde"> <span class="label-text">Notificar intento de desborde</span>
                </label>
                <button type="button" class="btn btn-primary btn-sm" data-placement="top" data-popover-content="#selectUser" data-toggle="popover" data-trigger="focus" v-if="form.notificarDesborde">
                    <i class="list-icon fa fa-user"></i>
                </button>
            </div>
        </div>
        <div class="d-none" id="selectUser">
            <div class="popover-body">
                <label>Usuarios a Notificar</label>
                <button type="button" class="btn btn-sm close" data-dismiss="alert"><i class="list-icon feather feather-x"></i></button>
                <select name="usuariosNotificar[]" class="form-control form-control-sm selectpicker show-tick">
                    <option value="">Seleccione a los usuarios</option>
                </select>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('js/vue/form/negocio/configuracionGeneralVue.js?version='.date('YmdHis'))!!}"></script>