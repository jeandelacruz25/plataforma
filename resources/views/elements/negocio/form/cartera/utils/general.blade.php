<div class="tabs">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link tabConfiguracionGeneral" href="#configuracion_general" data-toggle="tab" aria-expanded="true">
                <span class="thumb-xxs">
                    <img class="list-icon" src="{{ asset('img/modules/configuracion.svg?version='.date('YmdHis')) }}"> Configuración General
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabTiempoEstados" href="#tiempo_estados" data-toggle="tab" aria-expanded="true">
                <span class="thumb-xxs">
                    <img class="list-icon" src="{{ asset('img/modules/tiempo.svg?version='.date('YmdHis')) }}"> Tiempo de Estados (Agentes)
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link tabSeleccionUsuario" href="#seleccion_usuarios" data-toggle="tab" aria-expanded="true">
                <span class="thumb-xxs">
                    <img class="list-icon" src="{{ asset('img/modules/user_config.svg?version='.date('YmdHis')) }}"> Selección de Usuarios
                </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link configuracionInbound" href="#configuracion_inbound" data-toggle="tab" aria-expanded="true">
                <span class="thumb-xxs">
                    <img class="list-icon" src="{{ asset('img/modules/call_inbound.svg?version='.date('YmdHis')) }}"> Configuración de Inbound
                </span>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="configuracion_general">
            @include('elements.negocio.form.cartera.utils.general.configuracion_general')
        </div>
        <div class="tab-pane" id="tiempo_estados">
            @include('elements.negocio.form.cartera.utils.general.tiempo_estados')
        </div>
        <div class="tab-pane" id="seleccion_usuarios">
            @include('elements.negocio.form.cartera.utils.general.seleccion_usuarios')
        </div>
        <div class="tab-pane" id="configuracion_inbound">
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.tabConfiguracionGeneral').click()
    })
</script>