<div id="blankCartera" class="widget-bg">
    <div class="widget-heading bg-score">
            <span class="widget-title my-0 color-white fs-15 fw-600">
                Carteras
            </span>
        <div class="pull-right">
            <a class="btn btn-success btn-sm">
                <i class="list-icon material-icons text-inverse">add</i>
            </a>
        </div>
    </div>
    <div class="widget-body border_fix">
        <table class="table table-bordered nowrap">
            <thead class="bg-primary">
            <tr>
                <th>ID</th>
                <th>Cartera</th>
                <th>Acciones</th>
            </tr>
            </thead>
        </table>
        <div class="widget-user-profile">
            <div class="profile-body">
                <hr class="profile-seperator">
                <div class="profile-user-description mb-5">
                    <h5>Aquí aparecerán las carteras del proveedor seleccionado</h5>
                </div>
            </div>
        </div>
    </div>
</div>