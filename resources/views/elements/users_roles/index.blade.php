<div class="widget-holder col-md-12">
    <div class="widget-bg">
        <div class="widget-body">
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link tabUsuarios" href="#Usuarios" data-toggle="tab" aria-expanded="true" onclick="loadUserDatatable()">
                            <span class="thumb-xxs">
                                <img class="list-icon" src="{{ asset('img/menu/users.svg?version='.date('YmdHis')) }}"> Usuarios
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tabPerfiles" href="#Perfiles" data-toggle="tab" aria-expanded="true" onclick="loadRoleDatatable()">
                            <span class="thumb-xxs">
                                <img class="list-icon" src="{{ asset('img/menu/roles.svg?version='.date('YmdHis')) }}"> Perfiles
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="Usuarios">
                        @include('elements.users.index', ['titleModule' => $titleUsersModule, 'iconModule' => $iconUsersModule])
                    </div>
                    <div class="tab-pane" id="Perfiles">
                        @include('elements.roles.index', ['titleModule' => $titleRolesModule, 'iconModule' => $iconRolesModule])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.tabUsuarios').click()
    })

    function loadUserDatatable(){
        dataTables('listUsers', '/listUsers')
        vmFront.nameRoute = ucwords('{{ $titleUsersModule }}')
    }
    function loadRoleDatatable(){
        dataTables('listRoles', '/listRoles')
        vmFront.nameRoute = ucwords('{{ $titleRolesModule }}')
    }
</script>