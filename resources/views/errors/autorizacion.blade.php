<div class="row">
    <div class="widget-holder col-md-12">
        <div class="widget-bg">
            <div class="alert alert-icon alert-danger border-danger alert-dismissible fade show" role="alert">
                <i class="material-icons list-icon">not_interested</i>
                <strong>No cuentas con las credenciales necesarias para acceder a este módulo.</strong>
            </div>
        </div>
    </div>
</div>