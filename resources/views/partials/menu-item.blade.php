@if ($item['submenu'] == [])
    <li v-if="menu.indexOf('{{ $item["vue_name"] }}') >= 0">
        <a class="thumb-xxs mr-b-0" href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $item['vue_name'] }}')">
            <img class="list-icon" src="{{ asset($item['icon'].'?version='.date('YmdHis')) }}" style="margin-left: -8px;"> <span class="hide-menu">{{ $item['name'] }} </span>
        </a>
    </li>
@else
    <li class="menu-item-has-children" v-if="menu.indexOf('{{ $item["vue_name"] }}') >= 0">
        <a class="thumb-xxs mr-b-0" href="javascript:void(0);">
            <img class="list-icon" src="{{ asset($item['icon'].'?version='.date('YmdHis')) }}" style="margin-left: -8px;"> <span class="@if($hide) hide-menu @endif">{{ $item['name'] }}</span>
        </a>
        <ul class="list-unstyled sub-menu">
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <li v-if="menu.indexOf('{{ $submenu["vue_name"] }}') >= 0">
                        <a class="thumb-xxs mr-b-0" href="javascript:void(0);" v-on:click="loadOptionMenu('{{ $submenu['vue_name'] }}')">
                            <img class="list-icon" style="width: 25px !important; height: 25px !important;" src="{{ asset($submenu['icon'].'?version='.date('YmdHis')) }}" > <span>{{ $submenu['name'] }} </span>
                        </a>
                    </li>
                @else
                    @include('partials.menu-item', [ 'item' => $submenu, 'hide' => false ])
                @endif
            @endforeach
        </ul>
    </li>
@endif