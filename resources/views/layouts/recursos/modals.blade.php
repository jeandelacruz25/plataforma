<!-- Modal Score -->
<div id="modalScore" class="modal fade" role="dialog">
    <div class="modal-dialog dialogScore"></div>
    <div class="modal-dialog dialogScoreSmall modal-sm"></div>
    <div class="modal-dialog dialogScoreLarge modal-lg"></div>
    <div class="modal-dialog dialogScoreExtraLarge modal-xl"></div>
</div>