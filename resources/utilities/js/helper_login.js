// Agrega por defecto el crsf token en las peticiones ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

$('#submitLogin').click(function() {
    $(this).html('<i class="fa fa-spin fa-spinner fa-fw fa-lg"></i> ' + textLoading)
})

$('#changeUsername').click(function() {
    $(this).html('<i class="fa fa-spin fa-spinner fa-fw fa-lg"></i> ' + textLoading)
})