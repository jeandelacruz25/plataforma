'use strict'

var vmFront = new Vue({
    el: '#wrapper',
    data: {
        userInformation: '',
        menu: [],
        utilitiesUser: {
            totalUsers: '',
            totalUsersMonth: '',
            lastUser: ''
        },
        nameRoute: ''
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                let response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        loadAllMethods: function() {
            this.getUserInformation()
            this.getUserMenu()
            this.loadOptionMenu('blankPage')
        },
        loadOptionMenu: async function (idMenu) {
            $('#container').html(`<div class="row"><div class="widget-holder col-md-12"><div class="widget-bg"><div class="widget-body"><div class="cssload-loader"></div></div></div></div></div>`)
            $('#container').html(await this.sendUrlRequest(`/${idMenu}`))
        },
        getUserInformation: async function () {
            let userInformation = await this.sendUrlRequest('/getUserInformation')
            this.userInformation = userInformation[0]
            socketNodejs.emit('createRoom', { nameRoom: nameRoom, userID: userInformation[0].id})
        },
        getUserMenu: async function () {
            let userMenu = await this.sendUrlRequest('/getUserMenu')
            let menu = this.menu
            userMenu.forEach(function(value) {
                menu.push(value.vue_name)
            })
        },
        getUtilitiesUser: async function() {
            let userUtilites = await this.sendUrlRequest('/getUtilitiesUser')
            this.utilitiesUser.lastUser = userUtilites.last.username
            this.utilitiesUser.totalUsers = userUtilites.total
            this.utilitiesUser.totalUsersMonth = userUtilites.total_month
        },
        clickProveedor: async function(idProveedor) {
            $('#loadingCartera').removeClass('d-none')
            $('#requestCartera').addClass('d-none')
            $('#blankCartera').html('<div class="widget-heading bg-score"><span class="widget-title my-0 color-white fs-15 fw-600">Carteras</span><div class="pull-right"><a class="btn btn-success btn-sm"><i class="list-icon material-icons text-inverse">add</i></a></div></div><div class="widget-body border_fix"><div class="widget-user-profile"><div class="profile-body"><hr class="profile-seperator"><div class="profile-user-description mb-5"><div class="cssload-loader"></div></div></div></div></div>')
            $('#requestCartera').html(await this.sendUrlRequest(`/requestCarteras`, { idProveedor: idProveedor }))
            $('#loadingCartera').addClass('d-none')
            $('#requestCartera').removeClass('d-none')
        }
    }
})