'use strict'

var vmUserUtilities = new Vue({
    el: '#userUtilities',
    data: {
        utilitiesUser: {
            totalUsers: '',
            totalUsersMonth: '',
            lastUser: ''
        }
    },
    mounted: function () {
        this.getUtilitiesUser()
    },
    methods: {
        sendUrlRequest: async function (urlModule, parameters = {}) {
            try {
                let response = await axios.post(`${urlModule}`, parameters)
                return response.data
            } catch (error) { return error.status }
        },
        getUtilitiesUser: async function() {
            let userUtilites = await this.sendUrlRequest('/getUtilitiesUser')
            this.utilitiesUser.lastUser = userUtilites.last.username
            this.utilitiesUser.totalUsers = userUtilites.total
            this.utilitiesUser.totalUsersMonth = userUtilites.total_month
        }
    }
})