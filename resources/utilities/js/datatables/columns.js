/**
 * Created by Carlos on 15/12/2017.
 *
 * [columnsDatatable description]
 * @routes El ID de la tabla
 * @return Devuelve las columnas a tomarse
 */
const columnsDatatable = (route) => {
    let columns = ''
    if (route === 'listRoles') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listUsers') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'avatar', name: 'avatar', orderable: false, searchable: false, width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'username', name: 'username'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listProveedores') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'avatar', name: 'avatar', orderable: false, searchable: false, width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    if (route === 'listCarteras') {
        columns = [
            {data: 'id', name: 'id', width: '20px'},
            {data: 'name', name: 'name'},
            {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'}
        ]
    }
    return columns
}