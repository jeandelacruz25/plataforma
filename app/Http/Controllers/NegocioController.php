<?php

namespace App\Http\Controllers;

use App\Http\Requests\formProveedorRequest;
use App\Http\Requests\formProveedorStatusRequest;
use App\Models\Carteras;
use App\Models\Proveedores;
use App\Models\ProveedoresVoip;
use App\Models\RoleUser;
use App\Models\Estados;
use App\Models\TipoCanales;
use App\Models\TipoCarteras;
use App\Models\TipoEstados;
use App\Models\TipoMarcaciones;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class NegocioController extends ScoreController
{
    public function index() {
        return view('elements/negocio/index')->with(array(
            'titleModule'   => 'Administrar Negocio',
            'iconModule'    => 'feather feather-briefcase'
        ));
    }

    public function listProveedores(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_proveedor_list       = $this->proveedor_list_query();
            $builderview                = $this->builderviewProveedor($query_proveedor_list);
            $outgoingcollection         = $this->outgoingcollectionProveedor($builderview);
            $list_proveedores           = $this->FormatDatatable($outgoingcollection);
            return $list_proveedores;
        }
    }

    protected function proveedor_list_query()
    {
        $proveedor_list_query = Proveedores::Select()
            ->with('status')
            ->get()
            ->toArray();
        return $proveedor_list_query;
    }

    protected function builderviewProveedor($proveedor_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($proveedor_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_proveedor']         = $query['id'];
            $builderview[$posicion]['avatar']               = $query['avatar'];
            $builderview[$posicion]['name']                 = ucwords(Str::lower($query['razon_social']));
            $builderview[$posicion]['status']               = $query['status']['nombre'];
            $builderview[$posicion]['status_background']    = $query['status']['color'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionProveedor($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $iconStatus = $view['status'] == 'Activo' ? 'done' : 'close';
            $outgoingcollection->push([
                'id'            => $view['id'],
                'avatar'        => '<span class="avatar thumb-xxs mr-b-0"><img class="rounded-circle" src="'.asset($view['avatar']).'?version='.Carbon::now()->format('YmdHis').'"></span>',
                'name'          => '<a href="javascript:void(0)" onclick="vmFront.clickProveedor('.$view['id_proveedor'].')"><strong>'.$view['name'].'</strong></a>',
                'action'        => '<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="responseModal('."'div.dialogScoreLarge','formProveedores','".$view['id_proveedor']."'".')" data-toggle="modal" data-target="#modalScore"><i data-toggle="tooltip" data-placement="left" title="Editar Proveedor" class="fa fa-edit"></i></a>
                                    <button type="button" class="btn '.$view['status_background'].' btn-circle btn-sm ripple" onclick="responseModal('."'div.dialogScore','formProveedoresStatus','".$view['id_proveedor']."'".')" data-toggle="modal" data-target="#modalScore"><i class="material-icons list-icon">'.$iconStatus.'</i></button>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formProveedores(Request $request){
        if($request->valueID == null){
            return view('elements/negocio/form/proveedor/form_proveedor')->with(array(
                'dataProveedor'         => '',
                'updateForm'            => false
            ));
        }else{
            $getProveedor = $this->getProveedor($request->valueID);
            return view('elements/negocio/form/proveedor/form_proveedor')->with(array(
                'dataProveedor'         => $getProveedor,
                'updateForm'            => true
            ));
        }
    }

    public function formProveedoresStatus(Request $request){
        $dataProveedor  = $this->getProveedor($request->valueID);
        $statusOptions = $this->getNotProveedorStatus($dataProveedor[0]['status']['id']);
        return view('elements/negocio/form/proveedor/form_proveedor_status')->with(array(
            'dataProveedor'     => $dataProveedor,
            'options'           => $statusOptions
        ));
    }

    public function getProveedor($id){
        $proveedor = Proveedores::Select()
            ->with('status')
            ->where('id', $id)
            ->get()
            ->toArray();

        return $proveedor;
    }

    public function getNotProveedorStatus($idStatus){
        $statusProveedor = Estados::Select()
            ->whereNotIn('id',[$idStatus])
            ->get()
            ->toArray();
        return $statusProveedor;
    }

    public function saveFormProveedor(formProveedorRequest $request){
        if ($request->isMethod('post')) {

            $nameDirectory = public_path('img/proveedores/');
            if(!File::exists($nameDirectory)){
                $this->makeDirectory($nameDirectory);
            }

            if($request->avatarOriginalProveedor){
                $srcAvatar = $request->avatarOriginalProveedor;
            }else{
                $srcAvatar = 'img/proveedores/'.$request->rucProveedor.'.png';
                Image::make($request->file('avatarProveedor')->getRealPath())->resize(128, 128)->save($nameDirectory.$request->rucProveedor.'.png');
            }

            $userCreate = $request->userCreate ? $request->userCreate : Auth::user()->id;
            $dateCreate = $request->dateCreate ? $request->dateCreate : Carbon::now();
            $statusID = $request->statusID ? $request->statusID : '1';

            $proveedorQuery = Proveedores::updateOrCreate([
                'id'   => $request->proveedorID
            ], [
                'avatar'            => $srcAvatar,
                'ruc'               => $request->rucProveedor,
                'razon_social'      => $request->nombreProveedor,
                'contacto'          => $request->emailProveedor,
                'telephone'         => $request->telefonoProveedor,
                'address_business'  => $request->direccionProveedor,
                'status_id'         => $statusID,
                'user_cre'          => $userCreate,
                'user_upd'          => Auth::user()->id,
                'created_at'        => $dateCreate,
                'updated_at'        => Carbon::now()
            ]);

            if($proveedorQuery){
                return ['message' => 'Success', 'datatable' => 'listProveedores'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function saveFormProveedorStatus(formProveedorStatusRequest $request){
        if ($request->isMethod('post')) {
            $proveedoresQuery = Proveedores::where('id', $request->proveedorID)
                ->update(['id_status' => $request->statusProveedor]);
            if($proveedoresQuery){
                return ['message' => 'Success', 'datatable' => 'listProveedores'];
            }
            return ['message' => 'Error'];
        }
        return ['message' => 'Error'];
    }

    public function requestCarteras(Request $request){
        return view('elements/negocio/cards/carteras')->with(array(
            'idProovedor' => $request->idProveedor
        ));
    }

    public function listCarteras(Request $request)
    {
        if ($request->isMethod('post')) {
            $query_cartera_list         = $this->cartera_list_query($request->idProveedor);
            $builderview                = $this->builderviewCartera($query_cartera_list);
            $outgoingcollection         = $this->outgoingcollectionCartera($builderview);
            $list_carteras              = $this->FormatDatatable($outgoingcollection);
            return $list_carteras;
        }
    }

    protected function cartera_list_query($idProveedor)
    {
        $proveedor_list_query = Carteras::Select()
            ->where('id_proveedor', $idProveedor)
            ->with('estado')
            ->get()
            ->toArray();
        return $proveedor_list_query;
    }

    protected function builderviewCartera($cartera_list_query,$type='')
    {
        $posicion = 0;
        $idList = 0;
        foreach ($cartera_list_query as $query) {
            $idList ++;
            $builderview[$posicion]['id']                   = $idList;
            $builderview[$posicion]['id_cartera']           = $query['id'];
            $builderview[$posicion]['name']                 = ucwords(Str::lower($query['cartera']));
            $builderview[$posicion]['status']               = $query['estado']['nombre'];
            $builderview[$posicion]['status_background']    = $query['estado']['color'];
            $posicion ++;
        }
        if(!isset($builderview)){
            $builderview = [];
        }
        return $builderview;
    }

    protected function outgoingcollectionCartera($builderview)
    {
        $outgoingcollection = new \Illuminate\Support\Collection;
        foreach ($builderview as $view) {
            $iconStatus = $view['status'] == 'Activo' ? 'done' : 'close';
            $outgoingcollection->push([
                'id'            => $view['id'],
                'name'          => '<a href="javascript:void(0)" onclick="vmNegocio.clickProveedor('.$view['id_cartera'].')"><strong>'.$view['name'].'</strong></a>',
                'action'        => '<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="responseModal('."'div.dialogScoreLarge','formProveedores','".$view['id_cartera']."'".')" data-toggle="modal" data-target="#modalScore"><i data-toggle="tooltip" data-placement="left" title="Editar Cartera" class="fa fa-edit"></i></a>
                                    <button type="button" class="btn '.$view['status_background'].' btn-circle btn-sm ripple" onclick="responseModal('."'div.dialogScore','formProveedoresStatus','".$view['id_cartera']."'".')" data-toggle="modal" data-target="#modalScore"><i class="material-icons list-icon">'.$iconStatus.'</i></button>',
            ]);
        }
        return $outgoingcollection;
    }

    public function formCarteras(Request $request){
        return view('elements/negocio/form/cartera/index')->with(array(
            'dataCartera'           => '',
            'updateForm'            => false,
            'getOptions'            => $this->getOptions()
        ));
    }

    public function getOptions(){
        $tipoCartera = TipoCarteras::Select()
                ->get()
                ->toArray();

        $tipoCanales = TipoCanales::Select()
            ->get()
            ->toArray();

        $tipoMarcaciones = TipoMarcaciones::Select()
            ->get()
            ->toArray();

        $proveedoresVoip = ProveedoresVoip::Select()
            ->get()
            ->toArray();

        $tipoEstados = TipoEstados::Select()
            ->get()
            ->toArray();

        $usuarios = RoleUser::whereHas('role', function ($query) {
                $query->where('name', '!=', 'Administrador');
            })
            ->with('user')
            ->get()
            ->toArray();

        $supervisores = RoleUser::whereHas('role', function ($query) {
                $query->where('name', '=', 'Supervisor');
            })
            ->with('user')
            ->get()
            ->toArray();

        $agentes = RoleUser::whereHas('role', function ($query) {
                $query->where('name', '=', 'Agente');
            })
            ->with('user')
            ->get()
            ->toArray();

        $options['tipoCartera'] = $tipoCartera;
        $options['tipoCanales'] = $tipoCanales;
        $options['tipoMarcaciones'] = $tipoMarcaciones;
        $options['proveedoresVoip'] = $proveedoresVoip;
        $options['tipoEstados'] = $tipoEstados;
        $options['usuarios'] = $usuarios;
        $options['supervisores'] = $supervisores;
        $options['agentes'] = $agentes;
        return $options;
    }

}
