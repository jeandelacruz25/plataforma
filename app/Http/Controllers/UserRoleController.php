<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserRoleController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        return view('elements/users_roles/index')->with(array(
            'titleUsersModule'       => 'Administrar Usuarios',
            'iconUsersModule'        => 'feather feather-user',
            'nameRouteUsers'         => 'administrar usuarios',
            'titleRolesModule'       => 'Administrar Perfiles',
            'iconRolesModule'        => 'feather feather-users',
            'nameRouteRoles'         => 'administrar perfiles',
        ));
    }
}
