<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Webklex\IMAP\Client;

class HomeController extends ScoreController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Cookie::queue('cookie_avatar', Auth::user()->avatar, 2147483647);
        Cookie::queue('cookie_username', Auth::user()->username, 2147483647);
        return view('home');
    }

    public function blankPage(Request $request)
    {
        $oClient = new Client([
            'host'          => 'sv80.ifastnet.com',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => true,
            'username'      => 'prueba@ferrocentroaguilar.com',
            'password'      => 'nuncamas',
        ]);

        $oClient->connect();

        $aMailboxes = $oClient->getFolders(true);

        //$autorize = $request->user()->authorizeRoles(['user', 'admin']);
        //if($autorize) return view('elements/blank');
        //return view('errors/autorizacion');
        return view('elements/blank')->with(array(
            'titleModule' => 'pagina en blanco',
            'mail'  => $aMailboxes
        ));
    }
}
