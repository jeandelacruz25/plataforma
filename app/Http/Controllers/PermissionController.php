<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PermissionController extends ScoreController
{
    public function __construct()
    {
        $this->middleware('auth');
    }
}
