<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formProveedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if(empty($this->avatarOriginalProveedor)){
            $rules = [
                'avatarProveedor'   => 'required|image',
                'ruc'               => 'required|numeric',
                'razon_social'      => 'required|alpha_num',
                'contacto'          => 'required|email',
                'telephone'         => 'required|numeric',
                'address_business'  => 'required|alpha_dash'
            ];
        }else{
            $rules = [
                'avatarOriginalProveedor'   => 'required',
                'ruc'                       => 'required|numeric',
                'razon_social'              => 'required|alpha_num',
                'contacto'                  => 'required|email',
                'telephone'                 => 'required|numeric',
                'address_business'          => 'required|alpha_dash',
                'proveedorID'               => 'required'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        if(empty($this->avatarOriginalProveedor)){
            $rules = [
                'avatarProveedor.required'      => 'Debes ingresar una imagen',
                'avatarProveedor.image'         => 'El archivo a subir debe ser una imagen',
                'ruc.required'                  => 'Debes ingresar un numero de ruc',
                'ruc.numeric'                   => 'El RUC solo debe contener números',
                'razon_social.required'         => 'Debes ingresar un nombre para el Proveedor',
                'razon_social.alpha_num'        => 'El nombre del Proveedor contiene solo letras y números',
                'contacto.required'             => 'Debes ingresar un usuario de contacto',
                'contacto.email'                => 'El usuario de contacto no es un email valido',
                'telephone.required'            => 'Debes ingresar un número de telefono',
                'telephone.numeric'             => 'El telefono solo debe ser numerico',
                'address_business.required'     => 'Debes ingresar una dirección del proveedor',
                'address_business.alpha_dash'   => 'La dirección del proveedor debe contener letras, numeros y signos'
            ];
        }else{
            $rules = [
                'avatarOriginalProveedor.required'  => 'No hay ningun avatar anterior al actual',
                'ruc.required'                      => 'Debes ingresar un numero de ruc',
                'ruc.numeric'                       => 'El RUC solo debe contener números',
                'razon_social.required'             => 'Debes ingresar un nombre para el Proveedor',
                'razon_social.alpha_num'            => 'El nombre del Proveedor contiene solo letras y números',
                'contacto.required'                 => 'Debes ingresar un usuario de contacto',
                'contacto.email'                    => 'El usuario de contacto no es un email valido',
                'telephone.required'                => 'Debes ingresar un número de telefono',
                'telephone.numeric'                 => 'El telefono solo debe ser numerico',
                'address_business.required'         => 'Debes ingresar una dirección del proveedor',
                'address_business.alpha_dash'       => 'La dirección del proveedor debe contener letras, numeros y signos',
                'proveedorID.required'              => 'El proveedorID donde se actualizara los datos no existe'
            ];
        }

        return $rules;
    }
}
