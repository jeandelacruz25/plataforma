<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nameRole'          =>  'required|alpha_spaces',
            'descriptionRole'   =>  'required|alpha_spaces'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'nameRole.required'                 => 'Debes ingresar un nombre de rol',
            'nameRole.alpha_spaces'             => 'El nombre del rol debe contener solo letras y espacios',
            'descriptionRole.required'          => 'Debes ingresar una descripcion para el rol',
            'descriptionRole.alpha_spaces'      => 'La descripción del rol debe contener solo letras y espacios'
        ];

        return $rules;
    }
}
