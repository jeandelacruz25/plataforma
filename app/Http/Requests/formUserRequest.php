<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class formUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(empty($this->userID)){
            $rules = [
                'password'          => 'required|alpha_num',
                'firstName'         => 'required|alpha_spaces',
                'secondName'        => 'required|alpha_spaces',
                'lastName'          => 'required|alpha_spaces',
                'lastSecondName'    => 'required|alpha_spaces',
                'typeIdentity'      => 'required',
                'numIdentity'       => 'required|numeric',
                'birthDay'          => 'required|date_format:Y-m-d',
                'email'             => 'required|email',
                'username'          => 'required|alpha',
                'roleUser'          => 'required'
            ];
        }else{
            $rules = [
                'firstName'         => 'required|alpha_spaces',
                'secondName'        => 'required|alpha_spaces',
                'lastName'          => 'required|alpha_spaces',
                'lastSecondName'    => 'required|alpha_spaces',
                'typeIdentity'      => 'required',
                'numIdentity'       => 'required|numeric',
                'birthDay'          => 'required|date_format:Y-m-d',
                'email'             => 'required|email',
                'username'          => 'required|alpha',
                'roleUser'          => 'required',
                'userID'            => 'required'
            ];
        }

        if($this->password != $this->passwordValidate){
            $rules = [
                'password'          => 'required|alpha_num'
            ];
        }

        if(!empty($this->roleID)){
            $rules = [
                'roleID'            => 'required'
            ];
        }

        return $rules;
    }

    public function messages()
    {
        if(empty($this->userID)) {
            $rules = [
                'password.required'             => 'Debes ingresar una contraseña con letras y números',
                'password.alpha_num'            => 'La contraseña solo puede contener letras y números',
                'firstName.required'            => 'Debes ingresar el primer nombre',
                'firstName.alpha_spaces'        => 'El primer nombre debe contener solo letras',
                'secondName.required'           => 'Debes ingresar el segundo nombre',
                'secondName.alpha_spaces'       => 'El segundo nombre debe contener solo letras',
                'lastName.required'             => 'Debes ingresar el primer apellido',
                'lastName.alpha_spaces'         => 'El primer apellido debe contener solo letras',
                'lastSecondName.required'       => 'Debes ingresar el segundo apellido',
                'lastSecondName.alpha_spaces'   => 'El segundo apellido debe contener solo letras',
                'typeIdentity.required'         => 'Debes escoger un tipo de documento',
                'numIdentity.required'          => 'Debes ingresar un numero de documento',
                'numIdentity.numeric'           => 'El numero de documento, cuenta con letras o signos',
                'birthDay.required'             => 'Debes ingresar una fecha de nacimiento',
                'birthDay.date_format'          => 'La fecha de nacimiento debe tener el formato : '.Carbon::now()->format('Y-m-d'),
                'email.required'                => 'Debes ingresar un email',
                'email.email'                   => 'No se esta ingresando un email valido',
                'username.required'             => 'Debes ingresar un nombre de usuario',
                'username.alpha'                => 'El nombre de usuario debe contener solo letras',
                'roleUser.required'             => 'Debes seleccionar un rol para el usuario'
            ];
        }else{
            $rules = [
                'firstName.required'            => 'Debes ingresar el primer nombre',
                'firstName.alpha_spaces'        => 'El primer nombre debe contener solo letras',
                'secondName.required'           => 'Debes ingresar el segundo nombre',
                'secondName.alpha_spaces'       => 'El segundo nombre debe contener solo letras',
                'lastName.required'             => 'Debes ingresar el primer apellido',
                'lastName.alpha_spaces'         => 'El primer apellido debe contener solo letras',
                'lastSecondName.required'       => 'Debes ingresar el segundo apellido',
                'lastSecondName.alpha_spaces'   => 'El segundo apellido debe contener solo letras',
                'typeIdentity.required'         => 'Debes escoger un tipo de documento',
                'numIdentity.required'          => 'Debes ingresar un numero de documento',
                'numIdentity.numeric'           => 'El numero de documento, cuenta con letras o signos',
                'birthDay.required'             => 'Debes ingresar una fecha de nacimiento',
                'birthDay.date_format'          => 'La fecha de nacimiento debe tener el formato : '.Carbon::now()->format('Y-m-d'),
                'email.required'                => 'Debes ingresar un email',
                'email.email'                   => 'No se esta ingresando un email valido',
                'username.required'             => 'Debes ingresar un nombre de usuario',
                'username.alpha'                => 'El nombre de usuario debe contener solo letras',
                'roleUser.required'             => 'Debes seleccionar un rol para el usuario',
                'userID.required'               => 'El userID donde se actualizara los datos no existe'
            ];
        }

        if($this->password != $this->passwordValidate){
            $rules = [
                'password.required'         => 'Debes ingresar una contraseña con letras y números',
                'password.alpha_num'        => 'La contraseña solo puede contener letras y números'
            ];
        }

        if(!empty($this->roleID)){
            $rules = [
                'roleID.required'           => 'El roleID donde se actualizara los datos no existe'
            ];
        }

        return $rules;
    }
}
