<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formProveedorStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'proveedorID'        => 'required',
            'statusProveedor'    => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        $rules = [
            'proveedorID.required'        => 'El userID donde se actualizara los datos no existe',
            'statusProveedor.required'    => 'Debes seleccionar un estado'
        ];

        return $rules;
    }
}
