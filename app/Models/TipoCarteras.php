<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCarteras extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_carteras';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_cartera',
    ];
}
