<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'proveedores';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;

    protected $fillable = [
        'id', 'avatar', 'ruc', 'razon_social', 'contacto', 'telephone', 'address_business', 'id_status', 'user_cre', 'user_upd', 'created_at', 'updated_at',
    ];

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }
}
