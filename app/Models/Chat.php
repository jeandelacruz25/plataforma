<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'chat';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;
    const CREATED_AT        = 'fecha_reg';
    const UPDATED_AT        = 'fecha_act';

    protected $fillable = [
        'mensaje', 'user_id', 'fecha_reg', 'fecha_act',
    ];

    public function users_chat()
    {
        return $this->hasOne('App\Models\User', 'id','user_id');
    }
}
