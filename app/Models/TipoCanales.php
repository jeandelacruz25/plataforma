<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCanales extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_canales';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_canal',
    ];
}
