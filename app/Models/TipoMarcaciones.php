<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMarcaciones extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_marcaciones';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'tipo_marcacion',
    ];
}
