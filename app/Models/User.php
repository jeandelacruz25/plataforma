<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection   = 'securitec';
    protected $table        = 'users';
    protected $primaryKey   = 'id';
    public $timestamps      = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'id_tipo_identidad', 'num_identidad', 'fecha_nacimiento', 'email', 'username', 'password', 'avatar', 'locale', 'session_id', 'id_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public function getFullNameAttribute() {
        return ucfirst($this->primer_nombre) . ' ' . ucfirst($this->primer_apellido);
    }

    public function roles()
    {
        return $this
            ->belongsToMany('App\Models\Role')
            ->withTimestamps();
    }

    public function permissions()
    {
        return $this
            ->belongsToMany('App\Models\Permission')
            ->withTimestamps();
    }

    public function identity(){
        return $this->hasOne('App\Models\TipoIdentidad', 'id','id_tipo_identidad');
    }

    public function status(){
        return $this->hasOne('App\Models\Estados', 'id','id_status');
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) return true;
        return false;
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) return true;
            }
        } else {
            if ($this->hasRole($roles)) return true;
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) return true;
        return false;
    }

    public function authorizePermissions($permissions)
    {
        if ($this->hasAnyPermission($permissions)) return true;
        return false;
    }

    public function hasAnyPermission($permissions)
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                if ($this->hasPermission($permission)) return true;
            }
        } else {
            if ($this->hasPermission($permissions)) return true;
        }
        return false;
    }

    public function hasPermission($permission)
    {
        if ($this->permissions()->where('name', $permission)->first()) return true;
        return false;
    }
}
