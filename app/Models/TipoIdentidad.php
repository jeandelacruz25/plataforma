<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoIdentidad extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_identidad';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'tipo_identidad',
    ];
}
