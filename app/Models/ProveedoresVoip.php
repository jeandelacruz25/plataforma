<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProveedoresVoip extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'proveedores_voip';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'proveedor_voip',
    ];
}
