<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEstados extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'tipo_estados';
    protected $primaryKey   = 'id';
    public    $timestamps   = false;

    protected $fillable = [
        'id', 'estado',
    ];
}
