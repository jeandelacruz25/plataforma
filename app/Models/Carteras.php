<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carteras extends Model
{
    protected $connection   = 'securitec';
    protected $table        = 'carteras';
    protected $primaryKey   = 'id';
    public    $timestamps   = true;
    const CREATED_AT        = 'fecha_reg';
    const UPDATED_AT        = 'fecha_act';

    protected $fillable = [
        'id', 'id_proveedor', 'cartera', 'desc_corto', 'id_tipocartera', 'id_tipomodalidadgestion', 'id_tiporesidencia', 'nivel_resultado', 'id_proveedorvoip', 'nro_canalesdesborde', 'notificaciones_desborde', 'ges_manual', 'ges_progresiva', 'ges_predictiva', 'notificaciones_tiempos', 'tiempo_acw', 'meta_mensual', 'user_reg', 'user_act', 'fecha_reg', 'fecha_act', 'id_estado',
    ];

    public function tipo(){
        return $this->hasOne('App\Models\TipoCarteras', 'id','id_tipocartera');
    }

    public function estado(){
        return $this->hasOne('App\Models\Estados', 'id','id_estado');
    }
}