<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {

    /* Routes Auth */
    Auth::routes();

    /* Routes Default */
    Route::get('/',                                     'HomeController@index')->name('home');
    Route::get('/home',                                 'HomeController@index')->name('home');
    Route::post('/getUserInformation',                  'ScoreController@getUserInformation')->name('getUserInformation');
    Route::post('/getUserMenu',                         'ScoreController@getUserMenu')->name('getUserMenu');
    Route::post('/getUtilitiesUser',                    'ScoreController@getUtilitiesUser')->name('getUtilitiesUser');

    /* Pruebas */
    Route::post('/blankPage',                           'HomeController@blankPage')->name('Inicio');

    /* RemoveAuthCookie */
    Route::get('/removeAuthCookie',                     'ScoreController@cookieAuthRemove')->name('removeAuthCookie');

    /* Route Avatar */
    Route::post('/formAvatar',                          'UserController@formAvatar')->name('formAvatar');
    Route::post('/saveFormAvatar',                      'UserController@saveFormAvatar')->name('saveFormAvatar');

    /* Route Users Roles */
    Route::post('/usersRole',                           'UserRoleController@index')->name('usersRole');

    /* Route User */
    Route::post('/listUsers',                           'UserController@listUsers')->name('listUsers');
    Route::post('/formUsers',                           'UserController@formUsers')->name('formUsers');
    Route::post('/formUsersMenu',                       'UserController@formUsersMenu')->name('formUsersMenu');
    Route::post('/formUsersStatus',                     'UserController@formUsersStatus')->name('formUsersStatus');
    Route::post('/saveFormUsers',                       'UserController@saveFormUsers')->name('saveFormUsers');
    Route::post('/saveFormUsersMenu',                   'UserController@saveFormUsersMenu')->name('saveFormUsersMenu');
    Route::post('/saveFormUsersStatus',                 'UserController@saveFormUsersStatus')->name('saveFormUsersStatus');

    /* Route Role */
    Route::post('/listRoles',                           'RoleController@listRoles')->name('listRoles');
    Route::post('/formRoles',                           'RoleController@formRoles')->name('formRoles');
    Route::post('/formRolesAssing',                     'RoleController@formRolesAssing')->name('formRolesAssing');
    Route::post('/formRolesMenu',                       'RoleController@formRolesMenu')->name('formRolesMenu');
    Route::post('/saveFormRoles',                       'RoleController@saveFormRoles')->name('saveFormRoles');
    Route::post('/saveFormRolesAssing',                 'RoleController@saveFormRolesAssing')->name('saveFormRolesAssing');
    Route::post('/saveFormRolesMenu',                   'RoleController@saveFormRolesMenu')->name('saveFormRolesMenu');

    /* Route Chat */
    Route::post('/chat',                                'ChatController@index')->name('chat');
    Route::post('/getChat',                             'ChatController@getChat')->name('getChat');
    Route::post('/saveChatMessage',                     'ChatController@saveChatMessage')->name('saveChatMessage');

    /* Route Negocio */
    Route::post('/negocio',                             'NegocioController@index')->name('negocio');
    Route::post('/listProveedores',                     'NegocioController@listProveedores')->name('listProveedores');
    Route::post('/formProveedores',                     'NegocioController@formProveedores')->name('formProveedores');
    Route::post('/formProveedoresStatus',               'NegocioController@formProveedoresStatus')->name('formProveedoresStatus');
    Route::post('/saveFormProveedor',                   'NegocioController@saveFormProveedor')->name('saveFormProveedor');
    Route::post('/saveFormProveedorStatus',             'NegocioController@saveFormProveedorStatus')->name('saveFormProveedorStatus');
    Route::post('/requestCarteras',                     'NegocioController@requestCarteras')->name('requestCarteras');
    Route::post('/listCarteras',                        'NegocioController@listCarteras')->name('listCarteras');
    Route::post('/formCarteras',                        'NegocioController@formCarteras')->name('formCarteras');
});